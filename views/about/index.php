<?php

use yii\helpers\Html;

?>
<div class="row bg-gray">
    <div class="container">
        <div class="col-md-6 top-10">
            <h4 class="bottom-0 h-peach">Who we are</h4>
            <h1 class="top-0 h-700 h-48 dark-blue">We’re a group of friends</h1>
            <h4 class="p-300 line-h-13 dark-blue">who happen to be experienced multi-disciplinary strategists,
                creatives, and developers with shared vision for what we do and how we do it.</h4>
        </div>
        <div class="col-md-6 pb-5 pt-5">
            <?= Html::img('@web/images/friends.png', ['class' => 'width-480 mob-img-300', 'alt' => 'Friends']) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row top-5">
        <h2 class="h-48 h-700 h-peach text-center h-37-mob">CLARITY, DIRECTION & SENSE OF URGENCY</h2>
        <div class="col-md-6 top-5 text-mob">
            <h2 class="dark-bluedark-blue">FINMETRIX EMPLOYS A LEAN CONSULTING MODEL (LCM) WHICH RESULTS IN MAXIMUM
                VALUE CREATION AT MINIMUM RISK.</h2>
            <h4 class="top-5">Lean Consulting Model (LCM) consists of the following components:</h4>
            <h4 class="p-300">A small high impact team of experts with unrelenting focus on value.</h4>
            <h4 class="p-300">Proven and time tested transformation methodology.</h4>
            <h4 class="p-300">These teams are supported by a proprietary analytic platform enabling continuous
                performance measurment.</h4>
        </div>
        <div class="col-md-6 pb-5 pt-0">
            <?= Html::img('@web/images/prvi.png', ['class' => 'width-480 mob-img-300', 'alt' => 'Image1']) ?>
        </div>
    </div>
    <hr class="style1">
    <div class="row top-5 bottom-5">
        <h2 class="h-48 h-700 h-peach text-center h-37-mob">FINMETRIX TRANSFORMATION APPROACH</h2>
        <div class="col-md-6 top-10">
            <?= Html::img('@web/images/drugi.png', ['class' => 'width-480 mob-img-300', 'alt' => 'Image2']) ?>
        </div>
        <div class="col-md-6 top-5 text-mob">
            <h2 class="dark-bluedark-blue">FINMETRIX APPROACH TO BUSINESS TRANSFORMATION IS BASED ON ELIMINATION OF
                WASTE FROM COMMERCIAL ECOSYSTEM LIBERATING SUBSTANTIONAL SHAREHOLDER VALUE.</h2>
            <h3 class="top-5 h-peach">Before</h3>
            <h4 class="p-300">Inadequate processes</h4>
            <h4 class="p-300">System/tools redundacy</h4>
            <h4 class="p-300">Inflated organization</h4>
            <h3 class="top-5 h-peach">Results</h3>
            <h4 class="p-300">Implement even-flow processes</h4>
            <h4 class="p-300">Align organization with customer requirements</h4>
            <h4 class="p-300">Systems follow process requirements</h4>
            <h3 class="top-5 h-peach">Value</h3>
            <h4 class="p-300">Eliminate waste from the system</h4>
            <h4 class="p-300">Excellence through countinuous improvement</h4>
            <h4 class="p-300">Lower operating costs</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row">
        <div class="jumbotron pb-3">
            <h2 class="h-48 h-700 dark-blue">We are proud to introduce our team of exceptional professionals!</h2>
        </div>
        <div class="col-md-4">
            <?= Html::img('@web/images/member01.jpg', ['class' => 'width-370 mob-img-300', 'alt' => 'Peat Miletic']) ?>
            <h3 class="text-center bottom-0">Peat Miletic</h3>
            <h4 class="text-center p-300 top-0 bottom-5">Founder CEO</h4>
        </div>
        <div class="col-md-4">
            <?= Html::img('@web/images/kerr.png', ['class' => 'width-370 mob-img-300', 'alt' => 'Kathy Kerr']) ?>
            <h3 class="text-center bottom-0">Kathy Kerr</h3>
            <h4 class="text-center p-300 top-0 bottom-5">Director</h4>
        </div>
        <div class="col-md-4">
            <?= Html::img('@web/images/member02.jpg', ['class' => 'width-370 mob-img-300', 'alt' => 'Branko Terzic']) ?>
            <h3 class="text-center bottom-0">Branko Terzic</h3>
            <h4 class="text-center p-300 top-0 bottom-5">Director</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= Html::img('@web/images/member03.jpg', ['class' => 'width-370 mob-img-300', 'alt' => 'Alex Terzic']) ?>
            <h3 class="text-center bottom-0">Alex Terzic</h3>
            <h4 class="text-center p-300 top-0 bottom-5">Director</h4>
        </div>
        <div class="col-md-4">
            <?= Html::img('@web/images/member04.jpg', ['class' => 'width-370 mob-img-300', 'alt' => 'Bob Orr']) ?>
            <h3 class="text-center bottom-0">Bob Orr</h3>
            <h4 class="text-center p-300 top-0 bottom-5">Director</h4>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<div class="row bg-gray">
    <div class="container">
        <div class="row">
            <h1 class="h-900 text-center h-48 dark-blue">Have an idea?</h1>
            <h3 class="text-center p-300 top-0">We want to work with you to create the really cool stuff.</h3>
            <div class="form-actions">
                <?= Html::a('Challenge us', '#', [
                    'id' => 'challenge-btn',
                    'class' => 'btn btn-primary btn-lg btn-peach'
                ]) ?>
            </div>
        </div>
        <div class="row top-5 bottom-5">
            <div class="col-md-5 text-center">
                <img src="/images/address.png" alt="" class="width-85 mob-50">
                <h2 class="h-900 dark-blue">Visit Us</h2>
                <h3 class="p-300">Independence Warf 470 Atlantic Ave, </br> Boston</h3>
            </div>
            <div class="col-md-2 text-center">
                <h2 class="dark-blue h-48">or</h2>
            </div>
            <div class="col-md-5 text-center">
                <img src="/images/call.png" alt="" class="width-85 mob-50">
                <h2 class="h-900 dark-blue">Call Us</h2>
                <h3 class="p-300">(1) 774.286.0263</h3>
            </div>
        </div>
    </div>
</div>
