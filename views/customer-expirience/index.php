<div class="row">
    <div class="col-md-12 container-img bg-image-customer">
        <div class="container">
            <div class="row top-5 bg-text">
                <div class="col-md-12">
                    <h1 class="h-51">Enterprises leveraging FinMetrix Customer Experience platform have a 360 degree
                        view of their performance both from the customers point of view and internally from within
                        enterprise operations</h1>
                </div>
            </div>
            <div class="row bg-text">
                <div class="col-md-12">
                    <h3 class="p-300">FinMetrix Customer Experience platform enables our clients to gather customer
                        feedback from all communication channels and store it in a centralized repository</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob top-10">Customer Survey Configurator</h1>
            <img src="/images/bubble.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-18 text-mob">
            <h4 class="p-300 pb-5 p-16">Easy to use graphical interface for questionnaire development</h4>
            <h4 class="p-300 pb-5 p-16">A collection of pre developed survey questions ready to be combined into
                questionnaires</h4>
            <h4 class="p-300 pb-5 p-16">Automated questionnaire deployment to any communication channel: website, call
                center agent (internal or external), mobile devices, etc.</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Focus Group Configurator</h1>
            <img src="/images/histogram.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-13 text-mob">
            <h4 class="p-300 pb-5 p-16">Easy to use graphical interface for focus group development</h4>
            <h4 class="p-300 pb-5 p-16">A collection of pre developed focus group questions ready to be used</h4>
            <h4 class="p-300 pb-5 p-16">Automated focus group deployment to any communication channel: website, call
                center agent (internal or external), mobile devices, etc.</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Customer Satisfaction Analytics</h1>
            <img src="/images/gis.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-18 text-mob">
            <h4 class="p-300 pb-5 p-16">Pre-built dashboards for customer experience measurement</h4>
            <h4 class="p-300 pb-5 p-16">Standard KPIs such as net promoter score NPS</h4>
            <h4 class="p-300 pb-5 p-16">Performance target setting functionality puts current performance in perspective
                of desired performance levels</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Integrated Data Store</h1>
            <img src="/images/piechart.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-15 text-mob">
            <h4 class="p-300 pb-5 p-16">Survey and focus group responses are time stamped and stored enabling trend over
                time analysis</h4>
            <h4 class="p-300 pb-5 p-16">Survey and focus group responses are stored and correlated to customer/prospect
                records enabling behavioral analysis</h4>
            <h4 class="p-300 pb-5 p-16">Data store architecture allows for aggregation at any level, drill-down,
                drill-up and lateral analysis</h4>
        </div>
    </div>
</div>
