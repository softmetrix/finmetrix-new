<div class="row">
    <div class="col-md-12 container-img bg-image-utility">
        <div class="container">
            <div class="row top-10 bg-text">
                <div class="col-md-12">
                    <h1 class="h-51 h-900">Utilities leverage FinMetrix solutions to achieve best in class
                        performance</h1>
                </div>
            </div>
            <div class="row bg-text">
                <div class="col-md-12">
                    <h3 class="p-300">FinMetrix performance management platforms integrate a plethora of internal
                        systems to provide unprecedented visibility of customer operations performance drivers.</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob top-10">Call Center Optimization</h1>
            <img src="/images/graph1.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-15 text-mob">
            <h4 class="p-300 pb-5 p-16">Pinpoint problem areas by implementing KPIs that matter</h4>
            <h4 class="p-300 pb-5 p-16">Measure performance at the lowest granularity level - individual calls</h4>
            <h4 class="p-300 pb-5 p-16">Analyze the data and find root causes of suboptimal performance</h4>
            <h4 class="p-300 pb-5 p-16">Implement improvements and measure the results</h4>
            <h4 class="p-300 pb-5 p-16">Asses the impact of implemented change and raise the performance bar</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Collections Optimization</h1>
            <img src="/images/graph2.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-8 text-mob">
            <h4 class="p-300 pb-5 p-16">Select a small number of KPIs that explain 90% of changes in the AR
                portfolio</h4>
            <h4 class="p-300 pb-5 p-16">Measure performance at the lowest granularity level - individual customer
                bills</h4>
            <h4 class="p-300 pb-5 p-16">Analyze the data and find indicators that show concentration of value</h4>
            <h4 class="p-300 pb-5 p-16">Integrate measurement and analysis with operations (systems and
                organization)</h4>
            <h4 class="p-300 pb-5 p-16">Implement improvements and measure the results</h4>
            <h4 class="p-300 pb-5 p-16">Asses the impact of implemented change and raise the performance bar</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Marketing/Customer Contact</h1>
            <img src="/images/graph3.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-15 text-mob bottom-5">
            <h4 class="p-300 pb-5 p-16">Multi dimensional market segmentation (demographic, psychographic, geographic,
                proximity to the network, etc.)</h4>
            <h4 class="p-300 pb-5 p-16">Multi-channel marketing campaign development and delivery (phone, IVR, text,
                mobile, web)</h4>
            <h4 class="p-300 pb-5 p-16">Feedback capture and marketing campaign effectiveness measurement</h4>
            <h4 class="p-300 pb-5 p-16">Campaign reassessment and realignment when necessary</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Customer Experience</h1>
            <img src="/images/graph4.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-10 text-mob">
            <h4 class="p-300 pb-5 p-16">Define customer segments</h4>
            <h4 class="p-300 pb-5 p-16">Capture customer feedback utilizing customer preferred communication
                channels</h4>
            <h4 class="p-300 pb-5 p-16">Store customer feedback in a single source of truth database</h4>
            <h4 class="p-300 pb-5 p-16">Set performance targets and measure customer satisfaction</h4>
            <h4 class="p-300 pb-5 p-16">Implement improvements and measure the results of your efforts</h4>
        </div>
    </div>
</div>
