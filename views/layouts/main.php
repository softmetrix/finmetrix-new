<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <!-- MENU START -->
    <nav class="navbar navbar-blue bottom-0">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">
                        <img alt="Brand" src="images/LogoWhite.png">
                    </a>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Solutions<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Page 1-1</a></li>
                            <li><a href="#">Page 1-2</a></li>
                            <li><a href="#">Page 1-3</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Industries<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Url::toRoute(['/energy-utilities']) ?>" class="m-top-10">Energy
                                    Utilities</a></li>
                            <hr class="style1 m-10">
                            <li><a href="<?= Url::toRoute(['/life-sciences']) ?>">Life Sciences</a></li>
                            <hr class="style1 m-10">
                            <li><a href="<?= Url::toRoute(['/customer-expirience']) ?>">Customer Experience</a></li>
                            <hr class="style1 m-10">
                            <li><a href="<?= Url::toRoute(['/real-estate']) ?>">Commercial Real Estate</a></li>
                            <hr class="style1 m-10">
                            <li><a href="<?= Url::toRoute(['/revenue-management']) ?>">Revenue Management</a></li>
                            <hr class="style1 m-10">
                            <li><a href="<?= Url::toRoute(['/professional-services']) ?>">Professional Services</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Insights<span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Page 1-1</a></li>
                            <li><a href="#">Page 1-2</a></li>
                            <li><a href="#">Page 1-3</a></li>
                        </ul>
                    </li>
                    <li><a href="<?= Url::toRoute(['/about']) ?>">About</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- MENU END -->
    <div class="container-fluid">
        <!-- Pages -->
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= date('Y') ?> Finmetrix, Inc. All rights reserved.</p>
        <p class="pull-right">
          <span class="text-black">
            <a href="#">Privacy policy</a> |
            <a href="#">Cookie policy</a> |
            <a href="#">Terms of service</a>
          </span>
        </p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
