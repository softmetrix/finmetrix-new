<div class="row">
    <div class="col-md-12 container-img bg-image-celgene">
        <div class="container">
            <div class="row top-10">
                <div class="col-md-12">
                    <h1 class="h-51 mob-32">Enterprises leveraging FinMetrix Life Sciences platform achieve total
                        visibility of their product development life cycle (PDLC)</h1>
                </div>
            </div>
            <div class="row bg-text">
                <div class="col-md-12">
                    <h3 class="p-300">FinMetrix Life Sciences platform enables our clients to have a holistic view of
                        their PDLC including ongoing and planned programs, finances and resource utilization.
                        All of this for pre-clinical and clinical programs whether independent investigator or
                        enterprise initiated.</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob top-10">Single source of truth</h1>
            <img src="/images/bubble.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-15 text-mob">
            <h4 class="p-300 pb-5 p-16">An star schema data warehouse is at the base of the platform</h4>
            <h4 class="p-300 pb-5 p-16">Program management, clinical trials, human resource and financial data are all
                correlated and integrated in one database </h4>
            <h4 class="p-300 pb-5 p-16">Likelihood of approval, probability of running and other value added information
                is input through functionality available in the platform</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Risk, and Early Warning</h1>
            <img src="/images/histogram.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-10 text-mob">
            <h4 class="p-300 pb-5 p-16">Early warning system alerts to missed or likely to be missed required milestones
                providing visibility to projects at risk within the portfolio</h4>
            <h4 class="p-300 pb-5 p-16">Early warning system allows for timely corrective actions avoiding costly FDA
                denials</h4>
            <h4 class="p-300 pb-5 p-16">Early warning system captures delays in patient recruitment and abandonment rate
                giving protocol leaders visibility into potential study delays</h4>
            <h4 class="p-300 pb-5 p-16">Early warning system provides classification functionality thus enabling root
                cause analysis and elimination of factors that cause delays and abandonment</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Product pipeline analysis and reporting</h1>
            <img src="/images/gis.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-10 text-mob">
            <h4 class="p-300 pb-5 p-16">Future view of risk adjusted drug development portfolio across geographies,
                therapeutic areas, diseases, indications, etc. for all clinical milestones</h4>
            <h4 class="p-300 pb-5 p-16">Forecast of regulatory milestones and stage gates by geography, therapeutic
                area, disease, indication, etc.</h4>
            <h4 class="p-300 pb-5 p-16">Current state of drug development portfolio, rate of patient recruitment, by
                study site and study phase as well as by therapeutic area, disease and indication</h4>
            <h4 class="p-300 pb-5 p-16">Current portfolio performance relative to targeted milestones and
                submissions</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Production planning and scheduling</h1>
            <img src="/images/piechart.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-10 text-mob">
            <h4 class="p-300 pb-5 p-16">Production planning based on forecasted clinical trial milestones, patient
                acquisition rate and total number of patients</h4>
            <h4 class="p-300 pb-5 p-16">Integration of production schedules with budget and resource utilization
                information to provide a holistic view of production pipeline across compounds</h4>
            <h4 class="p-300 pb-5 p-16">Synchronization between clinical trials and production of drug substance,
                delivery mechanism and dosage is all accomplished within a single analytical platform</h4>
            <h4 class="p-300 pb-5 p-16">Tight integration with source systems ensures real time analysis and timely
                decisions</h4>
        </div>
    </div>
</div>
