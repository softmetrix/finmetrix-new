<div class="row">
    <div class="col-md-12 container-img bg-image-professional">
        <div class="container">
            <div class="row top-5 bg-text">
                <div class="col-md-12">
                    <h1 class="h-51">OUR CLIENTS HAVE BIG AUDACIOUS GOALS</h1>
                </div>
            </div>
            <div class="row bg-text">
                <div class="col-md-12">
                    <h3 class="p-300 top-5">They strive to achieve excelence in everything they do. They partner with
                        FinMetrix to design, develop and implement software and transform operations</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="container">
        <h1 class="h-51 text-center mob-32">FINMETRIX EMPLOYS A LEAN CONSULTING MODEL MINIMIZING RISK AND MAXIMIZING
            BENEFITS</h1>
        <hr class="top-5 style1">
    </div>
</div>
<div class="container">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob top-10">Pharma Product Pipeline Analysis and Repearing</h1>
            <img src="/images/bubble.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-10 text-mob">
            <ul>
                <li class="bullet">Rapid, iterative software development</li>
                <li class="bullet">Frequent user requirements collection</li>
                <li class="bullet">Rapid new version releases</li>
                <li class="bullet" class="bullet" class="bullet" class="bullet">Rapid MVP development and release</li>
                <li class="bullet" class="bullet" class="bullet">Useable software platform in less than 2 months</li>
                <li class="bullet" class="bullet">End to end visibility of drug development process</li>
                <li class="bullet">Data integration of multiple data sources</li>
            </ul>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob text-center">Utility</h1>
            <img src="/images/histogram.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-10 text-mob">
            <ul>
                <li class="bullet" class="bullet" class="bullet" class="bullet" class="bullet">Deep understanding of
                    performance drivers
                </li>
                <li class="bullet" class="bullet" class="bullet" class="bullet">Carefully designed key performance
                    indicators revealing hidden operational issues
                </li>
                <li class="bullet" class="bullet" class="bullet">Data correlation from diverse systems yielding
                    unprecedented insights
                </li>
                <li class="bullet" class="bullet">Ability to act on findings directly from the BI platform</li>
                <li class="bullet">Integration with geospatial data to increase operational agility</li>
            </ul>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Order to Cash</h1>
            <img src="/images/gis.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-10 text-mob">
            <ul>
                <li class="bullet" class="bullet" class="bullet" class="bullet" class="bullet">Relentless focus on
                    unlocking and releasing value
                </li>
                <li class="bullet" class="bullet" class="bullet" class="bullet">Automated portfolio segmentation based
                    on experience and machine learning
                </li>
                <li class="bullet" class="bullet" class="bullet">Hand in hand organizational change implementation</li>
                <li class="bullet" class="bullet">Disproportionate returns on investment from FinMetrix lead programs
                </li>
                <li class="bullet">Series of gradual changes all focused on a well-defined strategic goal</li>
            </ul>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob text-center">Customer</h1>
            <img src="/images/piechart.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-15 text-mob">
            <ul>
                <li class="bullet" class="bullet">Servey Jini – Interactive survey configurator</li>
                <li class="bullet">Multichannel customer reach (web, phone, mobile, text)</li>
                <li class="bullet" class="bullet" class="bullet">Neuroscientific customer experience analysis</li>
                <li class="bullet" class="bullet">Neuro testing of consumer behavior</li>
                <li class="bullet">Customer experience testing for website, mobile app, advertisement design and
                    implementation
                </li>
            </ul>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Continuous Improvement</h1>
            <img src="/images/gis.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-10 text-mob">
            <ul>
                <li class="bullet" class="bullet" class="bullet" class="bullet" class="bullet">Systemically implement
                    continuous improvement
                </li>
                <li class="bullet" class="bullet" class="bullet" class="bullet">Identify and eliminate friction in the
                    supply chain
                </li>
                <li class="bullet" class="bullet" class="bullet">Implement business rules based error elimination and
                    resolution
                </li>
                <li class="bullet" class="bullet">Eliminate waste from the extended supply chain</li>
                <li class="bullet">Institutionalize continuous improvement practices</li>
            </ul>
        </div>
    </div>
</div>
