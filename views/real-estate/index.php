<div class="row">
    <div class="col-md-12 container-img bg-image-estate">
        <div class="container">
            <div class="row top-5 bg-text">
                <div class="col-md-12">
                    <h1 class="h-51">Enterprises leveraging FinMetrix Commercial Real estate platform have end to end
                        visibility of their investment portfolio combined with Valuation, document management and
                        workflow functionality</h1>
                </div>
            </div>
            <div class="row bg-text">
                <div class="col-md-12">
                    <h3 class="p-300">FinMetrix Commercial Real Estate platform enables our clients to have a holistic
                        view of their REIT portfolio</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob top-10">Portfolio of properties</h1>
            <img src="/images/bubble.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-13 text-mob">
            <h4 class="p-300 pb-5 p-16">Property information, financials and human resources data are combined in a
                single source of truth star schema database</h4>
            <h4 class="p-300 pb-5 p-16">Financial performance metrics are computed at the lowest level of granularity
                (single tenant occupancy) and then rolled up at any desired level of aggregation</h4>
            <h4 class="p-300 pb-5 p-16">Performance measurement relative to goals of individual tenants, funds, fund
                managers and the entire portfolio are at finger tips</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob text-center">Valuation</h1>
            <img src="/images/histogram.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-10 text-mob">
            <h4 class="p-300 pb-5 p-16">Three different valuation methods are available in the web user interface</h4>
            <h4 class="p-300 pb-5 p-16">Historical asset performance is included in the valuation module for ease of
                comparison</h4>
            <h4 class="p-300 pb-5 p-16">Valuation tasks are assigned to valuators via the workflow functionality</h4>
            <h4 class="p-300 pb-5 p-16">Once a valuation is submitted it is committed to the database and is not
                editable at that point</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Document management</h1>
            <img src="/images/gis.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-15 text-mob">
            <h4 class="p-300 pb-5 p-16">Document management leverages Microsoft SharePoint and provides all its
                functionalities </h4>
            <h4 class="p-300 pb-5 p-16">Functionalities such as: indexing, document editing, collaboration, versioning
                are all available in the platform</h4>
            <h4 class="p-300 pb-5 p-16">Document store is available from any point of the platform</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob text-center">Workflow</h1>
            <img src="/images/piechart.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-13 text-mob">
            <h4 class="p-300 pb-5 p-16">Workflow is leveraging Microsoft Flow functionality allowing configuration of
                any real world process flow</h4>
            <h4 class="p-300 pb-5 p-16">Workflow is permeating the platform virtually enforcing the optimal process
                flow</h4>
            <h4 class="p-300 pb-5 p-16">Authorization levels, task assignments and approvals are all managed through the
                workflow functionality</h4>
        </div>
    </div>
</div>
