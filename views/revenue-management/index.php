<div class="row">
    <div class="col-md-12 container-img bg-image-flow">
        <div class="container">
            <div class="row top-5 bg-text">
                <div class="col-md-12">
                    <h1 class="h-51">Enterprises leveraging FinMetrix collections platform have cash pouring from their
                        accounts receivable</h1>
                </div>
            </div>
            <div class="row bg-text">
                <div class="col-md-12">
                    <h3 class="p-300">FinMetrix Collections Management platform enables our clients to focus resources
                        to areas of biggest cash flow impact while simultaneously maintaining high customer service
                        levels</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob top-10">Cash Collections Target Setting</h1>
            <img src="/images/bubble.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-20 text-mob">
            <h4 class="p-300 pb-5 p-16">Transaction level likelihood of collections and forecast</h4>
            <h4 class="p-300 pb-5 p-16">Portfolio level aggregation (collector, customer, business unit, company, all
                any other applicable)</h4>
            <h4 class="p-300 pb-5 p-16">Performance targets based on cash flow release</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob text-center">Dispute Management</h1>
            <img src="/images/histogram.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-10 text-mob">
            <h4 class="p-300 pb-5 p-16">LEAN methodology implemented for quality management in finance organization</h4>
            <h4 class="p-300 pb-5 p-16">Errors types are captured, together with cycle times </h4>
            <h4 class="p-300 pb-5 p-16">Root cause analysis is possible because of error type capture</h4>
            <h4 class="p-300 pb-5 p-16">Success of corrective actions implemented is continually monitored and
                evaluated</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-6 col-md-6">
            <h1 class="h-48 h-mob">Receivables Portfolio Segmentation</h1>
            <img src="/images/gis.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-md-6 top-15 text-mob">
            <h4 class="p-300 pb-5 p-16">Multi dimensional receivables segmentation (cash concentration, risk, payment
                performance)</h4>
            <h4 class="p-300 pb-5 p-16">Collections strategy optimized through segmentation (personal versus automated
                customer contact)</h4>
            <h4 class="p-300 pb-5 p-16">Optimal account allocation and organization based on concentration of value</h4>
        </div>
    </div>
    <hr class="style1">
    <div class="row feature">
        <div class="col-sm-push-6 col-sm-6 col-md-6">
            <h1 class="h-48 h-mob text-center">Performance Measurement</h1>
            <img src="/images/piechart.png" alt="" class="width-480 mob-img">
        </div>
        <div class="col-sm-6 col-sm-pull-6 col-md-6 top-15 text-mob">
            <h4 class="p-300 pb-5 p-16">Daily cash collections monitoring relative to daily cumulative targets allows
                for timely activity modification</h4>
            <h4 class="p-300 pb-5 p-16">Performance dashboards are aggregated at all levels of the organization</h4>
            <h4 class="p-300 pb-5 p-16">KPIs for different organizational levels are codependent, thus employees are
                aware of their contribution to overall performance</h4>
            <h4 class="p-300 pb-5 p-16">Real time bad debt forecasting and monitoring</h4>
        </div>
    </div>
</div>
