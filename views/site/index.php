<?php

use yii\helpers\Url;

?>
<div class="row">
    <div class="container">
        <div class="jumbotron">
            <p class="p-300">Don’t just measure! Achieve value accretion with an integrated
                performance improvement platform!</p>
            <h1 class="h-light-blue h-900 h-51">BE INFORMED, UNDERSATAND CAUSALITY, MESURE RESULTS</h1>
            <p class="p-300">Discover how FinMetrix performance management platform releases trapped value</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 container-img padding-0px padding-right-9px resp-img">
        <div class="modal-open">
            <a href="<?= Url::toRoute(['/energy-utilities']) ?>" class="img-link">
                <img src="/images/utility1.png" alt="" class="img-responsive hov-img">
                <div class="bottom-left left-align-30">
                    <h1>Energy Utilities</h1>
                    <h4 class="p-300">Achieve a 360° view of customer relationship management</h4>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-6 container-img padding-0px padding-left-9px resp-img">
        <div class="modal-open">
            <a href="<?= Url::toRoute(['/life-sciences']) ?>" class="img-link">
                <img src="/images/celgene1.jpg" alt="" class="img-responsive hov-img">
                <div class="bottom-left left-align-30">
                    <h1>Life Sciences</h1>
                    <h4 class="p-300">Control product life cycle process from portfolio to study phase level</h4>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="row st-1">
    <div class="col-md-6 container-img padding-0px padding-right-9px resp-img">
        <div class="modal-open">
            <a href="<?= Url::toRoute(['/customer-expirience']) ?>" class="img-link">
                <img src="/images/customer1.jpg" alt="" class="img-responsive hov-img">
                <div class="bottom-left left-align-30">
                    <h1>Customer Experience</h1>
                    <h4 class="p-300">Capture customer feedback in real time</h4>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-6 container-img padding-0px padding-left-9px resp-img">
        <div class="modal-open">
            <a href="<?= Url::toRoute(['/real-estate']) ?>" class="img-link">
                <img src="/images/estate1.jpg" alt="" class="img-responsive hov-img">
                <div class="bottom-left left-align-30">
                    <h1>Commercial Real Estate</h1>
                    <h4 class="p-300">Achieve valuation visibility from portfolio of assets to individual tenants</h4>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="row st-1">
    <div class="col-md-6 container-img padding-0px padding-right-9px resp-img">
        <div class="modal-open">
            <a href="<?= Url::toRoute(['/revenue-management']) ?>" class="img-link">
                <img src="/images/flow1.jpg" alt="" class="img-responsive hov-img">
                <div class="bottom-left left-align-30">
                    <h1>Revenue Management</h1>
                    <h4 class="p-300">Liberate cash trapped in past due receivables</h4>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-6 container-img padding-0px padding-left-9px resp-img">
        <div class="modal-open">
            <a href="<?= Url::toRoute(['/professional-services']) ?>" class="img-link">
                <img src="/images/professional1.jpg" alt="" class="img-responsive hov-img">
                <div class="bottom-left left-align-30">
                    <h1>Professional Services</h1>
                    <h4 class="p-300">Form nothing to daisy</h4>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="row top-5 bottom-5">
    <div class="container">
        <div class="col-md-6 blue-text">
            <h1 class="h-light-blue h-900 h-51">BUSINESS LEADERS LEVERAGE FINMETRIX SOLUTIONS
                TO REALIZE TANGIBLE BENEFITS RAPIDLY</h1>
            <h4 class="p-300">FinMetrix performance management platforms provide visibility,
                causality and focus on value accretion.</h4>
        </div>
        <div class="col-md-6 m-top-10">
            <img src="/images/graph.jpg" alt="" class="img-responsive">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 container-img padding-9px bg-image-solution">
        <div class="container">
            <div class="row top-5 bg-text">
                <div class="col-md-12">
                    <h1 class="h-51">FINMETRIX SOLUTIONS PROVIDE RAZOR SHARP FOCUS ON BUSINESS VALUE DRIVERS</h1>
                </div>
            </div>
            <div class="row bg-text">
                <div class="col-md-12">
                    <h3 class="p-300 line-h-13">Identify sources of value, focus on what matters most first,
                        </br> launch change initiatives, measure results, repeat</h3>
                </div>
            </div>
            <div class="row top-20">
                <div class="col-md-8">
                    <div class="col-md-4">
                        <img src="/images/target1.png" alt="" class="m-icons">
                        <h4>Marketing</h4>
                    </div>
                    <div class="col-md-4">
                        <img src="/images/faucet1.png" alt="" class="m-icons">
                        <h4>Revenue menagment</h4>
                    </div>
                    <div class="col-md-4">
                        <img src="/images/product1.png" alt="" class="m-icons">
                        <h4>Product management</h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-6">
                        <img src="/images/chart1.png" alt="" class="m-icons">
                        <h4>Customer contact management</h4>
                    </div>
                    <div class="col-md-6">
                        <img src="/images/city1.png" alt="" class="m-icons">
                        <h4>Property portfolio management</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
